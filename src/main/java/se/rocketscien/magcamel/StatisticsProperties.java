package se.rocketscien.magcamel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.sql.DataSourceDefinitions;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "camel.props")
public class StatisticsProperties {

    private CounterProps counter;

    private TimerProps timer;

    @Getter
    @Setter
    public static class CounterProps {

        private String all;

        private String xml;

        private String txt;

        private String invalid;

    }

    @Getter
    @Setter
    public static class TimerProps {

        private String all;

    }

}
