package se.rocketscien.magcamel.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import se.rocketscien.magcamel.model.TxtFile;
import se.rocketscien.magcamel.repo.TxtRepository;

@Service
@AllArgsConstructor
public class TxtService {

    private final TxtRepository txtRepository;

    public void saveTxt(String content) {
        txtRepository.save(TxtFile.content(content));
    }

}
