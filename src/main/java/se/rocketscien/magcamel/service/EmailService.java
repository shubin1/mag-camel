package se.rocketscien.magcamel.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class EmailService {

    public void sendEmailReport(
            double xmlCount,
            double txtCount,
            double unknownCount,
            double timeInMillis
    ) {
        log.info("--------------");
        log.info("Xml: {}, Txt: {}, Unknown: {}, Time: {}", xmlCount, txtCount, unknownCount, timeInMillis / 1000);
        log.info("--------------");
    }

}
