package se.rocketscien.magcamel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@Table("TXT_CONTENT")
public class TxtFile {

    @Id
    private Long id;

    private String content;

    public static TxtFile content(String content) {
        return new TxtFile(null, content);
    }

    public TxtFile withId(Long id) {
        return new TxtFile(id, content);
    }

}
