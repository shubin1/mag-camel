package se.rocketscien.magcamel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJdbcRepositories
@EnableTransactionManagement
@SpringBootApplication
@ImportResource(locations = { "classpath:context.xml" })
public class MagCamelApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagCamelApplication.class, args);
    }

}
