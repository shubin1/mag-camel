package se.rocketscien.magcamel;

import bitronix.tm.TransactionManagerServices;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.apache.camel.component.micrometer.MicrometerConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;

@Configuration
public class Config {

    @Bean(name = MicrometerConstants.METRICS_REGISTRY_NAME)
    public MeterRegistry getMeterRegistry() {
        MeterRegistry meterRegistry = new SimpleMeterRegistry();
        return meterRegistry;
    }

}
