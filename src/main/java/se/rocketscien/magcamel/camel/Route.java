package se.rocketscien.magcamel.camel;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.micrometer.MicrometerConstants;
import org.apache.camel.language.simple.SimplePredicateParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.rocketscien.magcamel.exception.InvalidFileException;

import java.util.concurrent.ExecutorService;

@Component
public class Route extends RouteBuilder {

    @Autowired
    private MeterRegistry meterRegistry;

    @Override
    public void configure() throws Exception {
        onException(InvalidFileException.class)
                .handled(true)
                .log("InvalidFileException occurred due: ${exception.message}");

        onException(Exception.class)
                .maximumRedeliveries(-1)
                .handled(false)
                .log("Exception occurred due: ${exception.message}")
                .to("micrometer:timer:${properties:camel.props.timer.all}?action=stop")
                .markRollbackOnly();

        from("file:data?move=done/${file:name}&readLock=markerFile&bridgeErrorHandler=true")
                .routeId("data")
                .tracing()
                .onCompletion()
                    .to("direct:complete")
                .end()
                .transacted("propagationPolicy")
                .to("micrometer:timer:${properties:camel.props.timer.all}?action=start")
                .choice()
                    .when()
                        .simple("${file:ext} == 'xml'")
                        .to("jms:queue:xml.q")
                        .to("micrometer:counter:${properties:camel.props.counter.xml}")
                    .when()
                        .simple("${file:ext} == 'txt'")
                        .to("jms:queue:txt.q")
                        .to("bean:txtService?method=saveTxt")
                        .to("micrometer:counter:${properties:camel.props.counter.txt}")
                    .otherwise()
                        .to("jms:queue:invalid-queue")
                        .to("micrometer:counter:${properties:camel.props.counter.invalid}")
                        .throwException(InvalidFileException.class, "${file:name} file");

        from("direct:complete")
                .to("micrometer:counter:${properties:camel.props.counter.all}")
                .to("micrometer:timer:${properties:camel.props.timer.all}?action=stop")
                .to("bean:statisticsService?method=evaluate");
    }
}
