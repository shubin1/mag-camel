package se.rocketscien.magcamel.camel;

import io.micrometer.core.instrument.*;
import org.apache.camel.Exchange;
import org.apache.camel.spring.boot.CamelConfigurationProperties;
import org.springframework.stereotype.Service;
import se.rocketscien.magcamel.StatisticsProperties;
import se.rocketscien.magcamel.service.EmailService;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class StatisticsService {

    public static final int THRESHOLD_AMOUNT = 100;

    private final MeterRegistry meterRegistry;
    private final EmailService emailService;
    private final StatisticsProperties statisticsProperties;
    private final List<Tag> tags;

    public StatisticsService(
            MeterRegistry meterRegistry,
            EmailService emailService,
            StatisticsProperties statisticsProperties,
            CamelConfigurationProperties camelConfigurationProperties
    ) {
        this.meterRegistry = meterRegistry;
        this.emailService = emailService;
        this.statisticsProperties = statisticsProperties;

        this.tags = Collections.singletonList(Tag.of("camelContext", camelConfigurationProperties.getName()));
    }

    public void evaluate(Exchange exchange) {
        Counter counter = meterRegistry.counter(statisticsProperties.getCounter().getAll(), tags);
        if (counter.count() <= THRESHOLD_AMOUNT) {
            return;
        }

        Counter xmlCounter = meterRegistry.counter(statisticsProperties.getCounter().getXml(), tags);
        Counter txtCounter = meterRegistry.counter(statisticsProperties.getCounter().getTxt(), tags);
        Counter invalidCounter = meterRegistry.counter(statisticsProperties.getCounter().getInvalid(), tags);
        Timer timer = meterRegistry.timer(statisticsProperties.getTimer().getAll(), tags);

        emailService.sendEmailReport(
                xmlCounter.count(),
                txtCounter.count(),
                invalidCounter.count(),
                timer.totalTime(TimeUnit.MILLISECONDS)
        );

        meterRegistry.remove(counter);
        meterRegistry.remove(xmlCounter);
        meterRegistry.remove(txtCounter);
        meterRegistry.remove(invalidCounter);
        meterRegistry.remove(timer);
    }

}
