package se.rocketscien.magcamel.exception;

public class InvalidFileException extends Exception {

    public InvalidFileException(String message) {
        super(message);
    }

}
