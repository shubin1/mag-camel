package se.rocketscien.magcamel.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.rocketscien.magcamel.model.TxtFile;

@Repository
public interface TxtRepository extends CrudRepository<TxtFile, Long> {
}
